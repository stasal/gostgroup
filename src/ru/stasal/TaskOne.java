package ru.stasal;

public class TaskOne {

    public static void main(String[] args) {
        for (byte i = 1; i <= 100; i++) {
            String message = "";
            if ((i & 1) == 0) {
                message += "Two";
            }

            if (i % 7 == 0) {
                message += "Seven";
            }

            if (message.isEmpty()) {
                message += i;
            }

            System.out.println(message);
        }
    }
}
