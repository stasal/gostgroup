package ru.stasal;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class TaskThree {
    public static void main(String[] args) {
        String text = args[0].toLowerCase();
        printWordOccurrecnes(text);
    }

    private static void printWordOccurrecnes(String text) {
        String[] words = text.split("[^\\p{L}0-9'\\n]+");
        Map<String, Integer> occurrences = new HashMap<>();
        for (String word : words) {
            occurrences.merge(word, 1, Integer::sum);
        }

        List<Map.Entry<String, Integer>> sortedEntries = sortEntriesByValue(occurrences);
        for (Map.Entry<String, Integer> occuranceEntry : sortedEntries) {
            System.out.println(occuranceEntry.getKey() + ": " + occuranceEntry.getValue());
        }
    }

    private static List<Map.Entry<String, Integer>> sortEntriesByValue(Map<String, Integer> occurancies) {
        List<Map.Entry<String, Integer>> result = new ArrayList<>(occurancies.entrySet());
        result.sort((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()));
        return result;
    }
}
