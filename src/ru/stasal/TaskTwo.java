package ru.stasal;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TaskTwo {
    public static void main(String[] args) {
        long m = Long.valueOf(args[0]);
        long r = Long.valueOf(args[1]);
        if (r > m) throw new IllegalArgumentException("m must be greater or equal to r");
        System.out.println(calculateFunction(m, r));
    }

    private static BigDecimal calculateFunction(long m, long r) {
        if (m == r) {
            return BigDecimal.ONE;
        }

        long diff = m - r;
        long max = Math.max(r, diff);
        long min = Math.min(r, diff);
        // для вычисления заданной функции достаточно разделить произведение чисел от максимального из r и (m-r)
        // а затем разделить его на факториал для минималього из этих чисел
        BigDecimal nominator = product(max + 1, m);
        BigDecimal denominator = factor(min);
        return nominator.divide(denominator, RoundingMode.HALF_UP);
    }

    private static BigDecimal factor(long value) {
        if (value == 0) return BigDecimal.ONE;
        return factor(value - 1).multiply(BigDecimal.valueOf(value));
    }

    private static BigDecimal product(long l, long m) {
        BigDecimal product = BigDecimal.ONE;
        for (long i = l; i <= m; i++) {
            product = product.multiply(BigDecimal.valueOf(i));
        }
        return product;
    }
}
